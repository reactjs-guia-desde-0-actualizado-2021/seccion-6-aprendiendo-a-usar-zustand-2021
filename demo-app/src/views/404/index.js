import { useContext } from "react";
import PokemonContext from "../../context/pokemons";

export default function FourOFour() {
  const { pokemons } = useContext(PokemonContext);
  console.log(pokemons);
  return (
    <div>
      <p>Perdona, no encontre lo que estabas buscando.</p>
    </div>
  );
}
